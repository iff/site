---
title: 'Cheatsheet'
pubDate: '2024-01-21'
---

Spring analogy for flexible structures:

- Bar, fixed in one end, deformation in the other end: $k=\frac{3EI}{l^3}$
- Bar, supported by two simple supports, deformation in the center: $k=\frac{48EI}{l^3}$
- Solid axle, deformation by torsion: $k=G\frac{\pi r^4}{2l}$

Equivalent rigidities:

- Spring in series: $k=\frac{k_1k_2}{k_1+k_2}$
- Spring in parallel: $k=k_1+k_2$

Lagrange's Equation:
$$
\frac{\dd}{\dd t} \left( \frac{\dd T}{\dd \dot u} \right) - \frac{\dd T}{\dd u} + \frac{\dd D}{\dd \dot u} + \frac{\dd U}{\dd u}=F
$$

Energies:
$$
T=\frac{1}{2}mv^2\quad;\quad D=\frac{1}{2}cv^2\quad;\quad U=\frac{1}{2}mu^2
$$

Frequencies:
$$
\omega_n=\sqrt{\frac{k}{m}}\quad;\quad f_n=\frac{1}{2\pi}\sqrt{\frac{k}{m}}
$$

Damping coefficient:
$$
\zeta=\frac{c}{c_{cr}}\quad;\quad c_{cr}=2m\omega_n=2\sqrt{km}
$$

- Can be measured by comparing amplitudes
	- Two consecutive amplitudes: $\zeta\approx\frac{1}{2\pi}\ln\frac{u_i}{u_{i+1}}$
	- $N$ cycles: $\zeta\approx\frac{1}{2\pi N}\ln\left( \frac{u_k}{u_{k+N}} \right)$

General solutions:
$$
\begin{aligned}
m\ddot u+ku=0 \quad & \rightarrow \quad u(t)=C_1\cos\omega_nt+C_2\sin\omega_nt\\
m\ddot u+c\dot u+ku=0 \quad& \rightarrow \quad u(t)=e^{\zeta\omega_n t}(C_1\cos\omega_d t+C_2\sin\omega_d t)
\end{aligned}
$$

- $\omega_d=\omega_n\sqrt{1-\zeta^2}\approx\omega_n$: Damped natural frequency

Fourier's series:
$$
\begin{aligned}
f(t)&=a_0+\sum_{n=1}^\infty \left( a_n\cos n\omega_0 t + b_n\sin n\omega_0t \right)\\
f(t)&=c_0+\sum_{n=1}^\infty c_n\cos(n\omega_0 t-\alpha_n)
\end{aligned}
$$
Coefficients:
$$
\begin{gathered}
a_0=\frac{1}{T_0}\int_0^{T_0}x(t)\dd t\quad;\quad a_n=\frac{2}{T_0}\int_0^{T_0}x(t)\cos n\omega t\dd t\quad;\quad b_n=\frac{2}{T_0}\int_0^{T_0} x(t)\sin n\omega t\dd t \\
c_n=\sqrt{a_n^2 + b_n^2} \quad;\quad \tan\alpha_n=\frac{b_n}{a_n}
\end{gathered}
$$
Power spectral density:
$$
\begin{aligned}
\langle x^2(t) \rangle&=\frac{1}{T}\int_0^Tx^2(t)\dd t\\
&=a_0^2+\frac{1}{2}\sum_1^\infty\left( a_n^2+b_n^2 \right)\\
&=c_0^2+\frac{1}{2}\sum_1^\infty c_n^2
\end{aligned}
$$

Data collecting parameters:
$$
\Delta f=\frac{f_s}{N}=\frac{1}{T}\quad;\quad \Delta t=\frac{T}{N}=\frac{1}{f_s}
$$

- Anti-aliasing condition: $f_s\ge f_{ny}=2f$
- Anti-leakage condition: $T_0/T\in\mathbb{Z}$

Modal analysis:
$$
\mathbf{M\ddot u} + \mathbf{C\dot u} + \mathbf{Ku} = \mathbf 0
$$

- To obtain natural frequencies:
	$$ \det\left( \mathbf K -\omega_n^2\mathbf M \right)=0 $$
- Modal vectors are the eigenvectors of the natural frequencies.
- Modal matrix is:
	$$ \mathbf\Phi=[\mathbf\phi_1, \mathbf\phi_2,\mathbf\phi_3] $$
	- Ordered from the lowest resonance frequency to the greatest.
	- If a resonance frequency is repeated, additional modal vectors can be obtained by:
		$$ \mathbf\phi_i^T\mathbf M\mathbf\phi_j=0 $$

Mapping to modal space:
$$
\mathbf{\mathcal M}=\mathbf\Phi^T\mathbf M\mathbf\Phi \quad;\quad \mathbf\eta= \mathbf\Phi^T\mathbf u
$$

