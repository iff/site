---
title: Discretization
pubDate: 2025-01-14
---

As computers cannot work with an infinite domain, discretization of the domain is required.

## Contents

## Spatial Discretization

There are different methods of spatial discretization:

- Finite differences: Based on differential equations
- Finite volumes: Based on integral equations
- Finite elements
- Spectral

Given the analytical solution in a time $t$ and defined in all values of $x$:
$$
u=u(x,t)
$$
Spatial discretization:

- Nodes: $x_i=(i-1)\Delta x$
	- $i=1,2,\dots,N$
	- $\Delta x=L/(N-1)$
	- Alternatively, $x_i=i\Delta x$ with $i=0,1,2,\dots,N+1$ where $\Delta x=L/(N+1)$
- Values: $u_i(t)=u(x_i,t)$
	- $i=1,2,\dots,N$
	- For simplicity, $u_i=u_i(t)$

Nodes $i\in\{1,N\}$ are boundary nodes, where values of $u_i$ should be imposed.

### Finite Differences

- Based on differentials
- Domain represented by a distribution of points (nodes)
- Derivatives are approximated with the linear combination of the values of variables being derived
- **Consistency condition**: The weight of each terms of the lineal combinations must be chosen such that they converge to the corresponding derivative when the nodes are infinitesimally close

Given a generic node $i$, its value is defined as $x_i=(i-1)\Delta x$ where $\Delta x=L/(N-1)$

- Internal nodes: $x_i\rightarrow i\in[1,N-1]$
	- Computational variables: $u_i\rightarrow i\in[1,N-1]$ for each time step
- External (boundary) nodes: $x_0=0,x_N=L$
	- Boundary conditions: $u_0,u_N$

Example of a **2nd order extrapolation**: $\tilde u_N=2u_{N-1}-u_{N-2}$:
$$
\begin{aligned}
\tilde u_N=2u_{N-1}-u_{N-2}\approx&2\left( u_N-\partial_xu_n \Delta x+\frac{1}{2}\partial_{xx}u_n\Delta x^2+\cdots \right)\\
&-\left( u_N-2\partial_xu_n\Delta x+\frac{1}{2}\partial_{xx}u_n(2\Delta x)^2+\cdots \right)\\
\approx&u_N-O(\Delta x^2)
\end{aligned}
$$

- Consistency: $\lim_{\Delta_x\rightarrow0}\tilde u_N=u_N$
- For simplicity, $u_N=\tilde u_N$

**2nd order central derivative**:
$$
\begin{gathered}
u_{i+1}=u_i+\Delta xu'_i+\frac{(\Delta x)^2}{2!}u_i''+\frac{(\Delta x)^3}{3!}u_i'''+\frac{(\Delta x)^4}{4!}u_i'''+\cdots\\
u_{i-1}=u_i-\Delta xu'_i+\frac{(\Delta x)^2}{2!}u_i''-\frac{(\Delta x)^3}{3!}u_i'''+\frac{(\Delta x)^4}{4!}u_i'''-\cdots\\
\end{gathered}
$$
Summing both expressions, we can only retain 2nd derivative:
$$
\begin{gathered}
u_{i+1}+u_{i-1}=2u_i+(\Delta x)^2u_i''+\frac{2}{4!}(\Delta x)^4u_i''''+\cdots\\
u_i''=\frac{u_{i+1}-2u_i+u_{i-1}}{(\Delta x)^2}-\frac{1}{12}(\Delta x)^2u_i''''+\cdots
\end{gathered}
$$
The dominant term scales with $\Delta x^2$, therefore we can define the second derivatives as:
$$
\mathcal D^2u_i=\frac{u_{i+1}-2u_i+u_{i-1}}{(\Delta x)^2}
$$

- Consistency: $\lim_{\Delta x\rightarrow0}\mathcal D^2u_i=u_i''$

A list of delayed, advanced, and centered discretizations:
$$
\begin{aligned}
&\mathcal Du_i\approx\frac{u_i-u_{i-1}}{\Delta x}\quad&;&\quad\mathcal D^2u_i\approx\frac{u_i-2u_{i-1}+u_{i-2}}{\Delta x^2}\\
&\mathcal Du_i\approx\frac{u_{i+1}-u_1}{\Delta x}\quad&;&\quad\mathcal D^2u_i\approx\frac{u_{i+2}-2u_{i+1}+u_i}{\Delta x^2}\\
&\mathcal Du_i\approx\frac{u_{i+1}-u_{i-1}}{2\Delta x}\quad&;&\quad\mathcal D^2u_i\approx\frac{u_{i+1}-2u_i+u_{i-1}}{\Delta x^2}\\
\end{aligned}
$$

### Finite Volumes

Both NS and Euler equations can be written in the conservative form:
$$
\frac{\partial\mathbf U}{\partial t}+\nabla\cdot\mathbf F=\mathbf S
$$

- $\mathbf U$: Vector of conservative variables
- $\mathbf F$: Vector of fluxes. Sum of convective $\mathbf F^c$ and viscous $\mathbf F^v$ fluxes 
	- $\mathbf F^v$ is also the sum of physical viscosity and numerical viscosity

Integrating the equation in a control volume $p$ gives:
$$
V_p\frac{\dd\overline{\mathbf U}_p}{\dd t}+\int_{V_p}\nabla\cdot\mathbf F\dd\mathbf V=\int_{V_p}\mathbf S\dd\mathbf V
$$

- $\overline{\mathbf U}_p=\frac{1}{V_p}\int_{\partial V_p}\mathbf U\dd\mathbf V$: Mean of the computational variable in the control volume $p$,

Using Gauss:
$$
V_p\frac{\dd\overline{\mathbf U}_p}{\dd t}+\oint_{\partial V_p}\mathbf F\cdot\dd\mathbf S=\int_{V_p}\mathbf S\dd\mathbf V
$$
Given the control volumes are plain faces, this can be simplified to:
$$
\frac{\dd\overline{\mathbf U}_p}{\dd t}+\frac{1}{V_p}\sum_{f\in\partial V_p}^\text{faces}\mathbf F_f\cdot\Delta\mathbf A_f=\overline{\mathbf S}
$$

## Temporal Discretization

Now considering the solution of any node $x_i$, in function of the time: $u_i(t)$. The time discretization is defined as:
$$
t_n=n\Delta t\quad;\quad n=0,1,2,\dots\quad;\quad\Delta t>0
$$

- $t_{n+1}=(n+1)\Delta t=t_n+\Delta t$
- The value of $u_i$ in time $t_n$ can be represented as $u_i^n$.

## Space-Time Discretization

Using the method of lines:

- Spatial discretization: $\partial_tu_i+c\partial_xu_i=0$
- Temporal discretization: $u_i^{n+1}=u_i^n+\Delta tF_i^n$
	- $F_i^n=-u_i\partial_xu_i$ at $t_n$
