---
title: Time Integrations
pubDate: 2025-01-15
---

Effective velocity, or phase velocity, for analytical and numerical time integration respectively:
$$
v_{ef}= \left| \frac{\lambda_I}{k} \right|=\left| \frac{\phi}{k\Delta t} \right|\quad;\quad\phi=\arctan \left( \frac{G_I}{G_R} \right)
$$

## Contents

## Explicit Euler

$$
\begin{gathered}
\mathbf u^{n+1}=\mathbf u^n+\Delta t\mathbf F(\mathbf u^n,t_n)\\
\left. \frac{\dd u}{\dd t} \right|_{t_n}=F(u^n,t_n)=F^n
\end{gathered}
$$
The gain is, assuming $F=\lambda u$:
$$
G=\frac{u^{u+1}}{u^n}=\frac{u^n+\Delta t\lambda u^n}{u^n}=1+\Delta t\lambda
$$

- 1st order scheme

## Implicit Euler

$$
\mathbf u^{n+1}=\mathbf u^n+\Delta t\mathbf F(\mathbf u^{n+1},t_{n+1})
$$

The gain is:
$$
u^{n+1}-\Delta\lambda u^{n+1}=u^n\quad \rightarrow \quad G=\frac{u^{n+1}}{u^n}=\frac{1}{1-\Delta t\lambda}
$$

- 1st order scheme

## Crank-Nicolson

Time discretization with $t_{n+1/2}$:
$$
\mathbf u^{n+1}=\mathbf u^n+\frac{\Delta t}{2}(\mathbf F(\mathbf u^n,t_n)+\mathbf F(\mathbf u^{n+1},t_{n+1}))
$$
The gain is:
$$
G=\frac{1+\Delta t\lambda/2}{1-\Delta t\lambda/2}
$$

- 2nd order scheme

## Rugen-Kutta Schemes

Multiphase scheme. Schemes from 2 to 4 phases:
$$
\begin{aligned}
\mathbf u^{n+1}&=\mathbf u^n+\frac{1}{2}(\mathbf k_1+\mathbf k_2)\\
\mathbf u^{n+1}&=\mathbf u^n+\frac{1}{6}(\mathbf k_1+4\mathbf k_2+\mathbf k_3)\\
\mathbf u^{n+1}&=\mathbf u^n+\frac{1}{6}(\mathbf k_1+2\mathbf k_2+2\mathbf k_3+\mathbf k_4)
\end{aligned}
$$
The gain depends on the number of phases:
$$
G=1+\Delta t\lambda+\frac{1}{2!}(\Delta t\lambda)^2+\frac{1}{3!}(\Delta t\lambda)^3+\frac{1}{4!}(\Delta t\lambda)^4
$$
