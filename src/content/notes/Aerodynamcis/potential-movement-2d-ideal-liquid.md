---
title: 2D Potential Movement of Ideal Liquids
pubDate: 2024-08-02
---

## Contents

## Complex Potential

When the number of Mach is small enough, the density $\rho$ can be considered constant. Supposing calm air, at large distances the field is uniform: Irrotational and the velocity derives from a potential:
$$
U=\frac{\partial \Phi}{\partial x}\quad;\quad W=\frac{\partial \Phi}{\partial z}
$$
Using this in the continuity equation we can obtain the differential equation for the potential:
$$
\nabla\Phi=\frac{\partial^2 \Phi}{\partial x^2}+\frac{\partial^2\Phi}{\partial z^2}=0
$$
which solution can be obtained using the following boundary conditions:

- Surface: $\mathbf V\cdot\mathbf n=0$
- Infinite: $U=U_\infty$ and $W=0$

Consider a function $f(t)$ with complex variable $t=x+\iu z$:
$$
\boxed{f(t)=\Phi(x,z)+\iu \Psi(x,z)}
$$

- $\Phi$: **Velocity potential**, represents the velocity potential of a movement in a plane
- $\Psi$: **Stream function**, which value is constant along streamlines. Streamlines are given by $\dd\Psi=U\dd z-W\dd x=0$

For this function to be derivable in a domain $D$, the 4 partial derivatives in the space must exist, and fulfil the Cauchy-Riemann conditions in $D$, and $f(t)$ being continuous in $D$:
$$
\frac{\dd f}{\dd t}=\dot f(t)=\frac{\partial \Phi}{\partial x}+\iu\frac{\partial \Psi}{\partial x}=-\iu \frac{\partial \Phi}{\partial z}+\frac{\partial \Psi}{\partial z}
$$
Which also gives:
$$
\frac{\partial \Phi}{\partial x}=\frac{\partial \Psi}{\partial z}\quad;\quad\frac{\partial \Phi}{\partial z}=-\frac{\partial \Psi}{\partial x}
$$
Also, both $\Phi$ and $\Psi$ are solutions of the Laplace's equation:
$$
\frac{\partial^2\Phi}{\partial x^2}+\frac{\partial^2\Phi}{\partial z^2}=0\quad;\quad
\frac{\partial^2\Psi}{\partial x^2}+\frac{\partial^2\Psi}{\partial z^2}=0
$$

### Properties

The derivative of the complex potential is the **conjugate velocity**:
$$
\frac{\dd f}{\dd t}=\frac{\partial\Phi}{\partial x}+\iu\frac{\partial \Psi}{\partial x}=U-\iu W
$$
Stagnation points can be obtained with $\dd f/\dd t=0$.

**Volumetric flow rate** in a 2D tube closed by two streamlines:
$$
G=\rho\int_A^B(U\dd z-W\dd x)=\rho\int_{\Psi_A}^{\Psi_B}\dd\Psi=\rho(\Psi_B-\Psi_A)
$$

### Elemental Solutions

**Uniform flow** with angle $\alpha$ with respect to the axis $x$:
$$
f(t)=U_\infty e^{-\iu\alpha}t
$$

**Source / Sink** with intensity $Q$ at point $t_0$:
$$
f(t)=\frac{Q}{2\pi}\ln(t-t_0)
$$

**Vortex** with intensity $\Gamma$ at $t_0$, positive when clockwise:
$$
f(t)=\frac{\iu\Gamma}{2\pi}\ln(t-t_0)
$$

**Doublet** with intensity $M$ with forms $\beta$ with axis $x$ at $t_0$:
$$
f(t)=\frac{Me^{\iu\beta}}{t-t_0}
$$

> Doublet is a singularity formed by a pair of source-sink or two vortices with opposite intensities.

## Stream of Ideal Liquid around a Circular Cylinder

### Without Circulation

Supposing a **doublet** in real axis ($\beta=0$) with intensity $a^2U_\infty$ and a **uniform stream** $U_\infty$ parallel to the real axis, the solution is a stream around a circular cylinder without circulation:
$$
f(t)=U_\infty\left[ t+\frac{a^2}{t} \right]
$$
with the imaginary part:
$$
\Psi(x,z)=U_\infty z\left[ 1-\frac{a^2}{x^2+z^2} \right]
$$

> The circle $x^2+z^2=a^2$ and real axis $z=0$ are streamlines.

The conjugate velocity is:
$$
\frac{\dd f}{\dd t}=U_\infty\left[ 1-\frac{a^2}{t^2} \right]
$$
with stagnation points at $t=\pm a$.

The **distribution of the pressure** over the cylinder, using $t=ae^{i\theta}$ and Bernoulli's equation:
$$
\begin{gathered}
\frac{\dd f}{\dd t}=U_\infty[1-\cos2\theta+\iu\sin2\theta]\quad\rightarrow\quad
U=2U_\infty\sin^2\theta \\
p(\theta)+\frac{1}{2}\rho U_\infty^24\sin^2\theta=p_\infty+\frac{1}{2}\rho U_\infty^2
\end{gathered}
$$
Commonly, the **pressure coefficient** is used:
$$
\boxed{c_p=\frac{p-p_\infty}{\frac{1}{2}\rho U_\infty^2}}=1-4\sin^2\theta
$$

> The **real distribution only appears close to the theoretical in the proximities of the front stagnation point** due to the existence of **viscosity and adverse pressure gradient**. As **turbulent boundary layers** (high Reynolds number) are less subjective to the adverse gradient and its separation is delayed, they are **more similar to the theoretical pressure** compared to a laminar layer.

### Cylinder with Circulation

Superposing any vortex in the origin, the cylinder described in the previous section is still a streamline. Now the complex potential:
$$
f(t)=U_\infty\left[ t+\frac{a^2}{t} \right]+\frac{\iu\Gamma}{2\pi}\ln\frac{t}{a}
$$
Stagnation points:
$$
\frac{\dd f}{\dd t}=\left( \frac{t}{a} \right)^2+\iu\frac{\Gamma}{2\pi aU_\infty}\frac{t}{a}-1=0
$$

> $\Gamma$ affects the position of the stagnation points. At the same time, it affects the velocity of the flow around the cylinder: When stagnation points get closer at one end, the velocity there is reduced (the enlarging zone has increased flow velocity).
>
> - $\Gamma=0$: Cylinder without circulation
> - $\Gamma<4\pi aU_\infty$: 2 stagnation points, closer to each other
> - $\Gamma=4\pi aU_\infty$: 1 stagnation point, in the cylinder
> - $\Gamma>4\pi aU_\infty$: 1 stagnation point, outside the cylinder
>
> By fixing stagnation points, the value of the vortex is defined. Furthermore, the vortex creates an asymmetry in the fluid field leading to lift.

## Circle Theorem

Given $f(t)$ a complex potential with source-sink, vortex, or doublet with a distance $>a$ to the origin, the complex potential due to such singularities plus a circular cylinder in the center of the origin with radius $a$ is:
$$
f(t)+\overline f\left( \frac{a^2}{t} \right)
$$

## Kutta-Joukowski Theorem: Forces over a Profile

Considering:

- Boundary of the profile formed by a distribution of singularities over a line element
- Using the conservation of momentum to a fluid volume containing the profile, which is faraway from the external frontiers

We have to calculate the upstream field induced by the profile. The velocity field induced by the profile in a uniform current contains:

- Non-perturbed stream
- Overlap of vortices between the leading edge and trailing edge, with an intensity per unit of length $\gamma(t_0)$
- Overlap of source-sink with intensity per unit of length $q(t_0)$

> If the profile has finite dimension (limited by a closed curve), then the sum of the intensities of the source-sink is 0. There isn't a known analogous property for vortices.

The complex potential is:
$$
\boxed{f(t)=U_\infty t+\frac{\iu}{2\pi}\int_{-c/2}^{c/2}\gamma(t_0)\ln(t-t_0)\dd l_0+\frac{1}{2\pi}\int_{-c/2}^{c/2}q(t_0)\ln(t-t_0)\dd l_0}
$$
Far away from the profile, we can make the assumption $\ln(t-t_0)\approx\ln t$, and using
$$
\Gamma=\int_{-c/2}^{c/2}\gamma(t_0)\dd l_0\quad;\quad
Q=\int_{-c/2}^{c/2}q(t_0)\dd l_0=0
$$
the complex potential and the conjugate velocity far away from the profile is:
$$
f(t)=U_\infty t+\frac{\iu\Gamma}{2\pi}\ln t \quad;\quad \frac{\dd f}{\dd t}=U_\infty+\frac{\iu\Gamma}{2\pi}\frac{1}{t}
$$
Considering the external frontier a circle with a large but finite radius $R$ and using the change of variable $x=R\cos\theta$ and $z=R\sin\theta$, the components of the velocity are: [^r2exclude]

$$
U=U_\infty+\frac{\Gamma}{2\pi R}\sin\theta\quad;\quad
W=-\frac{\Gamma}{2\pi R}\cos\theta
$$

the pressure faraway:
$$
p=p_\infty-\frac{1}{2}\rho\frac{U_\infty\Gamma}{\pi R}\sin\theta
$$
The mass flux across a differential element of the external frontier is $\rho RU_\infty\cos\theta\dd\theta+O(1/R)$, the horizontal and vertical components of the momentum flux are:
$$
\rho RU_\infty\cos\theta\left[ U_\infty+\frac{\Gamma}{2\pi R}\sin\theta \right]\dd\theta\quad;\quad
\rho RU_\infty\cos\theta\left[ -\frac{\Gamma}{2\pi R}\cos\theta \right]\dd\theta
$$
The pressure force over the external frontier, without take into account $p_\infty$:
$$
p_x=\frac{1}{2}\rho\frac{\Gamma}{\pi R}U_\infty\cos\theta\sin\theta R\dd\theta\quad;\quad
p_z=\frac{1}{2}\rho\frac{\Gamma}{\pi R}U_\infty\sin^2\theta R\dd\theta
$$
The action of the obstacle over the fluid (which has the same value but opposite sign with the force we desire to know) are therefore:
$$
\begin{aligned}
\rho RU_\infty\int_0^{2\pi}\left( U_\infty\cos\theta + \frac{\Gamma}{2\pi R}\cos\theta\sin\theta \right)\dd\theta&=\rho RU_\infty\int_0^{2\pi}\frac{\Gamma}{2\pi R}\cos\theta\sin\theta\dd\theta-d\\
\rho RU_\infty\int_0^{2\pi}-\frac{\Gamma}{2\pi R}\cos^2\theta\dd\theta&=\rho RU_\infty\int_0^{2\pi}\frac{\Gamma}{2\pi R}\sin^2\theta\dd\theta-l
\end{aligned}
$$

Results:

- $d=0$: **D'Alambert's paradox**
	- Won't happen. In a real stream there is separation of the boundary layer and the stagnation point disappears, leading to friction and resistances. Thus, vortices appears despite having no vorticity.
- $l=\rho\Gamma U$: **Kutta-Joukowski's formula**
	- To have lift, $\Gamma\ne0$

**Kutta-Joukowski's theorem**: The circulation around a profile must be appropriate in the way that the stagnation point is in the trailing edge. Types of trailing edge:

- Sharp: Velocities cannot be same in modulus and direction, the condition only fulfil when their value are zeros
- Smooth: Velocity must be the same and non-zero, removing the trailing point

[^r2exclude]: Terms of $O(1/R^2)$ are excluded
