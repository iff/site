import rss from '@astrojs/rss'
import { getCollection } from 'astro:content'
import { SITE_TITLE, SITE_DESCRIPTION } from '../consts'

export async function GET(context) {
	const posts = (await getCollection('posts'))
		.sort((a, b) => {
			const bDate =
				(b.data.pubDate && b.data.pubDate.valueOf()) || new Date(0).valueOf()
			const aDate =
				(a.data.pubDate && a.data.pubDate.valueOf()) || new Date(0).valueOf()
			return bDate - aDate
		})
		.slice(0, 10)

	return rss({
		title: SITE_TITLE,
		description: SITE_DESCRIPTION,
		site: context.site,
		items: posts.map((post) => ({
			...post.data,
			link: `/post/${post.slug}/`,
		})),
	})
}
