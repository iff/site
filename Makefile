.DEFAULT_GOAL := open

open:
	npm run dev -- --open

serve:
	npm run build
	npm run preview

build:
	npm run build

pretty:
	npm run pretty
